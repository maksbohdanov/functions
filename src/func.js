const getSum = (str1, str2) => {
  str1 = str1 === "" ? 0 : str1;
  str2 = str2 === "" ? 0 : str2;

  var num1 = parseInt(str1);
  var num2 = parseInt(str2);

  if(isNaN(num1) || isNaN(num2)){
    return false;
  }
  else{
    return (num1 + num2).toString();
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const filteredPost = listOfPosts.filter(function(item) {
    if(item['author'] === authorName) {
      return true;
    }
  })
  const postCount = filteredPost.length
  
  let counter = 0
  listOfPosts.forEach(function(item) {
    const comments = item['comments']
    if (comments !== undefined){
      comments.forEach(function(item2) {
         if(item2['author'] === authorName) {
            counter++
          } 
      })
    }
  })  
  
  return `Post:${postCount},comments:${counter}`
};

const tickets=(people)=> {
  if(Number(people[0] != 25)){
    return 'NO';
  }
  var cash = 25;
  for(var i = 1; i < people.length; i++){
    cash += 25 - (Number(people[i])-25);
    if(cash < 0)
    {
      return 'NO';
    }    
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
